<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Todo\Model\Todo;
use App\User;
use Faker\Generator as Faker;

$factory->define(Todo::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'user_id' => function () {
            return factory(User::class)->create();
        },
        'complete' => false
    ];
});
