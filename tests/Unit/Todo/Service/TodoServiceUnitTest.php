<?php

namespace Tests\Unit\Todo\Service;

use App\Todo\Contract\Service\TodoServiceInterface;
use App\Todo\Model\Todo;
use App\User;
use Tests\TestCase;

class TodoServiceUnitTest extends TestCase
{
    public function test_it_can_get_todo_list()
    {
        $user = factory(User::class)->create();
        factory(Todo::class, 5)->create(['user_id' => $user->id]);
        $todoService = app(TodoServiceInterface::class);

        $result = $todoService->get($user->id);
        $this->assertNotNull($todoService);
        $this->assertNotNull($result);
        $this->assertCount(5, $result);
        $this->assertInstanceOf(TodoServiceInterface::class, $todoService);
    }

    public function test_it_can_get_todo_by_id()
    {
        $user = factory(User::class)->create();
        factory(Todo::class, 5)->create(['user_id' => $user->id]);
        $todoService = app(TodoServiceInterface::class);

        $result = $todoService->find(3);
        $dbresult = Todo::find(3);
        $this->assertNotNull($todoService);
        $this->assertNotNull($result);
        $this->assertNotNull($dbresult);
        $this->assertInstanceOf(Todo::class, $result);
        $this->assertInstanceOf(TodoServiceInterface::class, $todoService);
        $this->assertEquals($dbresult->name, $result->name);
        $this->assertEquals($dbresult->id, $result->id);
    }

    public function test_it_can_create_todo()
    {
        $fakeTodo = factory(Todo::class)->make();
        $todoService = app(TodoServiceInterface::class);

        $createdTodo = $todoService->create(
            $fakeTodo->name,
            $fakeTodo->user_id);
        $this->assertNotNull($todoService);
        $this->assertNotNull($createdTodo);
        $this->assertInstanceOf(TodoServiceInterface::class, $todoService);
        $this->assertInstanceOf(Todo::class, $createdTodo);
        $this->assertEquals($fakeTodo->name, $createdTodo->name);
        $this->assertEquals($fakeTodo->user_id, $createdTodo->user_id);
    }

    public function test_it_can_update_todo_status_to_complete()
    {
        $fakeTodo = factory(Todo::class)->create();
        $todoService = app(TodoServiceInterface::class);

        $updatedTodo = $todoService->complete($fakeTodo->id, true);
        $fakeTodo = Todo::find($fakeTodo->id);
        $this->assertNotNull($todoService);
        $this->assertNotNull($updatedTodo);
        $this->assertInstanceOf(TodoServiceInterface::class, $todoService);
        $this->assertInstanceOf(Todo::class, $updatedTodo);
        $this->assertEquals($fakeTodo->name, $updatedTodo->name);
        $this->assertEquals(true, $updatedTodo->complete);
    }

    public function test_it_can_update_todo_status_to_incomplete()
    {
        $fakeTodo = factory(Todo::class)->create();
        $todoService = app(TodoServiceInterface::class);

        $updatedTodo = $todoService->complete($fakeTodo->id, true);
        $updatedTodo = $todoService->complete($fakeTodo->id, false);
        $fakeTodo = Todo::find($fakeTodo->id);
        $this->assertNotNull($todoService);
        $this->assertNotNull($updatedTodo);
        $this->assertInstanceOf(TodoServiceInterface::class, $todoService);
        $this->assertInstanceOf(Todo::class, $updatedTodo);
        $this->assertEquals($fakeTodo->name, $updatedTodo->name);
        $this->assertEquals(false, $updatedTodo->complete);
    }

    public function test_it_can_delete_todo()
    {
        $fakeTodo = factory(Todo::class)->create();
        $todoService = app(TodoServiceInterface::class);

        $result = $todoService->delete($fakeTodo->id);
        $fakeTodo = Todo::find($fakeTodo->id);

        $this->assertNotNull($todoService);
        $this->assertTrue($result);
        $this->assertNull($fakeTodo);
        $this->assertInstanceOf(TodoServiceInterface::class, $todoService);
    }
}
