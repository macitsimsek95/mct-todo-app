<?php

namespace Tests\Unit\Todo\Event;

use App\Todo\Event\CompleteTodo\CompleteTodoEvent;
use App\Todo\Event\CompleteTodo\CompleteTodoEventListener;
use App\Todo\Event\CreateTodo\CreateTodoEvent;
use App\Todo\Event\CreateTodo\CreateTodoEventListener;
use App\Todo\Event\DeleteTodo\DeleteTodoEvent;
use App\Todo\Event\DeleteTodo\DeleteTodoEventListener;
use App\Todo\Event\TodoCompleted\TodoCompletedEvent;
use App\Todo\Event\TodoCreated\TodoCreatedEvent;
use App\Todo\Event\TodoDeleted\TodoDeletedEvent;
use App\Todo\Event\TodoInCompleted\TodoInCompletedEvent;
use App\Todo\Model\Todo;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class EventUnitTest extends TestCase
{

    public function test_it_creates_todo_when_create_todo_event_dispatch()
    {
        Event::fake();

        $fakeTodo = factory(Todo::class)->make();

        $event = \Mockery::mock(CreateTodoEvent::class);
        $event->name = $fakeTodo->name;
        $event->user_id = $fakeTodo->user_id;


        $listener = $this->app->make(CreateTodoEventListener::class);
        $listener->handle($event);

        $createdTodo = Todo::whereName($event->name)->first();

        $this->assertNotNull($createdTodo);
        $this->assertInstanceOf(Todo::class, $createdTodo);
        $this->assertEquals($event->name, $createdTodo->name);
        $this->assertEquals($event->user_id, $createdTodo->user_id);
        Event::assertDispatched(TodoCreatedEvent::class);
    }

    public function test_it_make_complete_when_complete_todo_event_dispatch()
    {
        Event::fake();

        $fakeTodo = factory(Todo::class)->create(['complete' => false]);

        $event = \Mockery::mock(CompleteTodoEvent::class);
        $event->id = $fakeTodo->id;


        $listener = $this->app->make(CompleteTodoEventListener::class);
        $listener->handle($event);

        $result = Todo::find($event->id);

        $this->assertNotNull($result);
        $this->assertInstanceOf(Todo::class, $result);
        $this->assertTrue($result->complete);
        Event::assertDispatched(TodoCompletedEvent::class);
    }

    public function test_it_make_complete_when_incomplete_todo_event_dispatch()
    {
        Event::fake();

        $fakeTodo = factory(Todo::class)->create(['complete' => true]);

        $event = \Mockery::mock(CompleteTodoEvent::class);
        $event->id = $fakeTodo->id;


        $listener = $this->app->make(CompleteTodoEventListener::class);
        $listener->handle($event);

        $result = Todo::find($event->id);

        $this->assertNotNull($result);
        $this->assertInstanceOf(Todo::class, $result);
        $this->assertFalse($result->complete);
        Event::assertDispatched(TodoInCompletedEvent::class);
    }

    public function test_it_deletes_when_delete_todo_event_dispatch()
    {
        Event::fake();

        $fakeTodo = factory(Todo::class)->create();

        $event = \Mockery::mock(DeleteTodoEvent::class);
        $event->id = $fakeTodo->id;


        $listener = $this->app->make(DeleteTodoEventListener::class);
        $listener->handle($event);

        $result = Todo::find($event->id);
        $this->assertNull($result);
        Event::assertDispatched(TodoDeletedEvent::class);
    }

    public function test_it_dispatches_the_todo_created_event()
    {
        Event::fake();

        $fakeTodo = factory(Todo::class)->create();

        \event(new TodoCreatedEvent($fakeTodo));

        Event::assertDispatched(TodoCreatedEvent::class);
    }

    public function test_it_dispatches_the_todo_deleted_event()
    {
        Event::fake();

        $fakeTodo = factory(Todo::class)->create();

        \event(new TodoDeletedEvent($fakeTodo->id));

        Event::assertDispatched(TodoDeletedEvent::class);
    }

    public function test_it_dispatches_the_todo_completed_event()
    {
        Event::fake();

        $fakeTodo = factory(Todo::class)->create();

        \event(new TodoCompletedEvent($fakeTodo->id));

        Event::assertDispatched(TodoCompletedEvent::class);
    }

    public function test_it_dispatches_the_todo_incompleted_event()
    {
        Event::fake();

        $fakeTodo = factory(Todo::class)->create();

        \event(new TodoInCompletedEvent($fakeTodo->id));

        Event::assertDispatched(TodoInCompletedEvent::class);
    }
}
