<?php

namespace Tests\Unit\User\Event;

use App\User;
use App\User\Event\CreateUser\CreateUserEvent;
use App\User\Event\CreateUser\CreateUserEventListener;
use App\User\Event\UpdateUser\UpdateUserEvent;
use App\User\Event\UpdateUser\UpdateUserEventListener;
use App\User\Event\UserCreated\UserCreatedEvent;
use App\User\Event\UserUpdated\UserUpdatedEvent;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class EventUnitTest extends TestCase
{

    public function test_it_creates_user_when_create_user_event_dispatch()
    {
        Event::fake();

        $fakeUser = factory(User::class)->make();

        $event = \Mockery::mock(CreateUserEvent::class);
        $event->name = $fakeUser->name;
        $event->email = $fakeUser->email;
        $event->password = $fakeUser->password;


        $listener = $this->app->make(CreateUserEventListener::class);
        $listener->handle($event);

        $createdUser = User::whereEmail($event->email)->first();

        $this->assertNotNull($createdUser);
        $this->assertInstanceOf(User::class, $createdUser);
        $this->assertEquals($event->email, $createdUser->email);
        Event::assertDispatched(UserCreatedEvent::class);
    }

    public function test_it_updates_user_when_update_user_event_dispatch()
    {
        Event::fake();

        $fakeUser = factory(User::class)->create();

        $event = \Mockery::mock(UpdateUserEvent::class);
        $event->user_id = $fakeUser->id;
        $event->name = $fakeUser->name . 'change';
        $event->email = $fakeUser->email . 'change';


        $listener = $this->app->make(UpdateUserEventListener::class);
        $listener->handle($event);

        $updatedUser = User::find($fakeUser->id);

        $this->assertNotNull($updatedUser);
        $this->assertInstanceOf(User::class, $updatedUser);
        $this->assertEquals($event->email, $updatedUser->email);
        Event::assertDispatched(UserUpdatedEvent::class);
    }

    public function test_it_dispatches_the_user_created_event()
    {
        Event::fake();

        $fakeUser = factory(User::class)->create();

        \event(new UserCreatedEvent($fakeUser));

        Event::assertDispatched(UserCreatedEvent::class);
    }

    public function test_it_dispatches_the_user_updated_event()
    {
        Event::fake();

        $fakeUser = factory(User::class)->create();

        \event(new UserUpdatedEvent($fakeUser));

        Event::assertDispatched(UserUpdatedEvent::class);
    }
}
