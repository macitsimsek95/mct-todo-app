<?php

namespace Tests\Unit\User\Service;

use App\User;
use App\User\Contract\Service\UserServiceInterface;
use Tests\TestCase;

class UserServiceUnitTest extends TestCase
{
    public function test_it_can_create_user()
    {
        $fakeUser = factory(User::class)->make();
        $userService = app(UserServiceInterface::class);
        $createdUser = $userService->create(
            $fakeUser->name,
            $fakeUser->email,
            'password');

        $this->assertNotNull($userService);
        $this->assertNotNull($createdUser);
        $this->assertInstanceOf(UserServiceInterface::class, $userService);
        $this->assertInstanceOf(User::class, $createdUser);
        $this->assertEquals($fakeUser->name, $createdUser->name);
        $this->assertEquals($fakeUser->email, $createdUser->email);
    }

    public function test_it_can_update_user()
    {
        $fakeUser = factory(User::class)->create();
        $userService = app(UserServiceInterface::class);
        $updatedUser = $userService->update($fakeUser->id,
            $fakeUser->name . 'changed',
            $fakeUser->email . 'changed', '');

        $fakeUser = User::find($fakeUser->id);
        $this->assertNotNull($userService);
        $this->assertNotNull($updatedUser);
        $this->assertInstanceOf(UserServiceInterface::class, $userService);
        $this->assertInstanceOf(User::class, $updatedUser);
        $this->assertEquals($fakeUser->name, $updatedUser->name);
        $this->assertEquals($fakeUser->email, $updatedUser->email);
    }
}
