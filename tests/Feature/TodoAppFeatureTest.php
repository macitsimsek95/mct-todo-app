<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;

class TodoAppFeatureTest extends TestCase
{
    public function test_it_can_see_welcome_text_in_home_page()
    {
        factory(User::class)->create();
        $this->be(User::first());
        $response = $this->get('/');
        $response->assertStatus(200);
        $response->assertSee('Welcome to Macit Simsek ToDo App');
    }

    public function test_it_can_see_login_page()
    {
        $response = $this->get('/login');
        $response->assertStatus(200);
        $response->assertSee('Sign In');
    }

    public function test_it_can_see_register_page()
    {
        $response = $this->get('/user/create');
        $response->assertStatus(200);
        $response->assertSee('Register New User');
    }

    public function test_it_can_see_todo_list_page()
    {
        factory(User::class)->create();
        $this->be(User::first());
        $response = $this->get('/todo');
        $response->assertStatus(200);
        $response->assertSee('Todo List');
    }
}
