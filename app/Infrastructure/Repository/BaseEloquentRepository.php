<?php


namespace App\Infrastructure;


use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class BaseEloquentRepository implements BaseRepositoryInterface
{

    /**
     * @var Model
     */
    protected $model;

    /**
     * BaseRepository constructor.
     * @param $model
     */
    public function __construct($model)
    {
        $this->model = $model;
    }

    /**
     * @param array $attributes
     * @return Model
     */
    public function create(array $attributes): Model
    {
        return $this->model::create($attributes);
    }

    /**
     * @param int $id
     * @param array $data
     * @return bool
     */
    public function update(int $id, array $data): bool
    {
        $model = $this->find($id);
        return $model->update($data);
    }

    /**
     * @param array $columns
     * @param string $orderBy
     * @param string $sortBy
     * @return Collection
     */
    public function all($columns = ['*'], string $orderBy = 'id', string $sortBy = 'asc'): Collection
    {
        return $this->model::orderBy($orderBy, $sortBy)->get($columns);
    }

    /**
     * @param string $column
     * @param string $columnValue
     * @return Collection
     */
    public function where(string $column, string $columnValue): Collection
    {
        return $this->model::where($column, $columnValue)->get();
    }

    /**
     * @param string $id
     * @return Model
     */
    public function find($id): Model
    {
        return $this->model::find($id);
    }

    /**
     * @param  $id
     * @return Model
     * @throws ModelNotFoundException
     */
    public function findOrFail($id): Model
    {
        return $this->model::findOrFail($id);
    }

    /**
     * @param array $data
     * @return Collection
     */
    public function findBy(array $data): Collection
    {
        return $this->model::where($data)->get();
    }

    /**it fetch records from db according to the id
     * @param int $id
     * @return Model
     */
    public function findById(int $id): Model
    {
        return $this->model::where('id', '=', $id)->first();
    }

    /**
     * @param array $data
     * @return Model
     */
    public function first(array $data): Model
    {
        return $this->model::where($data)->first();
    }

    /**
     * @param array $data
     * @return Model
     * @throws ModelNotFoundException
     */
    public function firstOrFail(array $data): Model
    {
        return $this->model::where($data)->firstOrFail();
    }

    /**
     * @param int $id
     * @return bool
     * @throws \Exception
     */
    public function delete(int $id): bool
    {
        $model = $this->find($id);
        return $model->delete();
    }
}
