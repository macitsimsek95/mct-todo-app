<?php


namespace App\Infrastructure;


use Illuminate\Database\Eloquent\Collection;

interface BaseRepositoryInterface
{
    /** it does create a record according to the array you will pass
     * @param array $attributes
     */
    public function create(array $attributes);

    /** it does update according to the array you will pass
     * @param int $id
     * @param array $attributes
     * @return bool
     */
    public function update(int $id, array $attributes): bool;

    /** it fetch all data from database
     * @param array $columns
     * @param string $orderBy
     * @param string $sortBy
     */
    public function all($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc');

    /**
     * @param string $column
     * @param string $columnValue
     * @return Collection
     */
    public function where(string $column, string $columnValue): Collection;

    /** it find just one record from db according to the id you will pass
     * @param $id
     */
    public function find($id);

    /**it find just one record from db according to the id you will pass
     * if it is doesn't fetch it will be throw exception
     * @param $id
     */
    public function findOrFail($id);

    /**it fetch records from db according to the conditions you will pass
     * @param array $data
     */
    public function findBy(array $data);

    /**it fetch records from db according to the id
     * @param int $id
     */
    public function findById(int $id);

    /**it fetch just one record from db according to the conditions you will pass
     * @param array $data
     */
    public function first(array $data);

    /**it fetch just one record from db according to the id you will pass
     * if it is doesn't fetch it will be throw exception
     * @param array $data
     */
    public function firstOrFail(array $data);

    /**it delete record from db according to the id you will pass
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool;
}
