<?php


namespace App\Todo\Repository;


use App\Infrastructure\BaseEloquentRepository;
use App\Todo\Contract\Repository\TodoRepositoryInterface;
use App\Todo\Model\Todo;

class TodoRepository extends BaseEloquentRepository implements TodoRepositoryInterface
{
    /**
     * TodoRepository constructor.
     */
    public function __construct()
    {
        parent::__construct(Todo::class);
    }
}
