<?php


namespace App\Todo\Event\TodoCreated;


use App\Todo\Contract\Service\TodoServiceInterface;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class TodoCreatedEventListener implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * @var TodoServiceInterface
     */
    private $todoService;

    /**
     * Create the event listener.
     *
     * @param TodoServiceInterface $todoService
     */
    public function __construct(TodoServiceInterface $todoService)
    {
        $this->todoService = $todoService;
    }

    /**
     * Handle the event.
     *
     * @param TodoCreatedEvent $event
     * @return void
     */
    public function handle(TodoCreatedEvent $event)
    {
        //we can connect to some third party service
    }
}
