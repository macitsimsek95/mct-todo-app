<?php


namespace App\Todo\Event\TodoDeleted;


use App\Todo\Contract\Service\TodoServiceInterface;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class TodoDeletedEventListener implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * @var TodoServiceInterface
     */
    private $todoService;

    /**
     * Create the event listener.
     *
     * @param TodoServiceInterface $todoService
     */
    public function __construct(TodoServiceInterface $todoService)
    {
        $this->todoService = $todoService;
    }

    /**
     * Handle the event.
     *
     * @param TodoDeletedEvent $event
     * @return void
     */
    public function handle(TodoDeletedEvent $event)
    {
        //handle what you want
    }
}
