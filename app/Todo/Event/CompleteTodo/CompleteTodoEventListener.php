<?php


namespace App\Todo\Event\CompleteTodo;


use App\Todo\Contract\Service\TodoServiceInterface;
use App\Todo\Event\TodoCompleted\TodoCompletedEvent;
use App\Todo\Event\TodoInCompleted\TodoInCompletedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CompleteTodoEventListener implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * @var TodoServiceInterface
     */
    private $todoService;

    /**
     * Create the event listener.
     *
     * @param TodoServiceInterface $todoService
     */
    public function __construct(TodoServiceInterface $todoService)
    {
        $this->todoService = $todoService;
    }

    /**
     * Handle the event.
     *
     * @param CompleteTodoEvent $event
     * @return void
     */
    public function handle(CompleteTodoEvent $event)
    {
        $todo = $this->todoService->find($event->id);
        $this->todoService->complete(
            $event->id,
            !$todo->complete
        );
        if (!$todo->complete){
            event(new TodoCompletedEvent($event->id));
        }else{
            event(new TodoInCompletedEvent($event->id));
        }

    }
}
