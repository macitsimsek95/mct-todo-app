<?php


namespace App\Todo\Event\CompleteTodo;


use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CompleteTodoEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }
}
