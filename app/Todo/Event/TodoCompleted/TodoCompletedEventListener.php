<?php


namespace App\Todo\Event\TodoCompleted;


use App\Todo\Contract\Service\TodoServiceInterface;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class TodoCompletedEventListener implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * @var TodoServiceInterface
     */
    private $todoService;

    /**
     * Create the event listener.
     *
     * @param TodoServiceInterface $todoService
     */
    public function __construct(TodoServiceInterface $todoService)
    {
        $this->todoService = $todoService;
    }

    /**
     * Handle the event.
     *
     * @param TodoCompletedEvent $event
     * @return void
     */
    public function handle(TodoCompletedEvent $event)
    {
        //we can fire with third party service in here
    }
}
