<?php


namespace App\Todo\Event\CreateTodo;


use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CreateTodoEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $name;
    public $user_id;

    public function __construct(string $name, int $user_id)
    {
        $this->name = $name;
        $this->user_id = $user_id;
    }
}
