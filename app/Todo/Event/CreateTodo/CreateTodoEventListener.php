<?php


namespace App\Todo\Event\CreateTodo;


use App\Todo\Contract\Service\TodoServiceInterface;
use App\Todo\Event\TodoCreated\TodoCreatedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CreateTodoEventListener implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * @var TodoServiceInterface
     */
    private $todoService;

    /**
     * Create the event listener.
     *
     * @param TodoServiceInterface $todoService
     */
    public function __construct(TodoServiceInterface $todoService)
    {
        $this->todoService = $todoService;
    }

    /**
     * Handle the event.
     *
     * @param CreateTodoEvent $event
     * @return void
     */
    public function handle(CreateTodoEvent $event)
    {
        $todo = $this->todoService->create(
            $event->name,
            $event->user_id
        );
        event(new TodoCreatedEvent($todo));
    }
}
