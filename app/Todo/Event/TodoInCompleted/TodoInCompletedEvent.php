<?php


namespace App\Todo\Event\TodoInCompleted;


use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class TodoInCompletedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $id;

    public function __construct(int $id)
    {
        $this->$id = $id;
    }
}
