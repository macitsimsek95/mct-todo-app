<?php


namespace App\Todo\Event\TodoInCompleted;


use App\Todo\Contract\Service\TodoServiceInterface;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class TodoInCompletedEventListener implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * @var TodoServiceInterface
     */
    private $todoService;

    /**
     * Create the event listener.
     *
     * @param TodoServiceInterface $todoService
     */
    public function __construct(TodoServiceInterface $todoService)
    {
        $this->todoService = $todoService;
    }

    /**
     * Handle the event.
     *
     * @param TodoInCompletedEvent $event
     * @return void
     */
    public function handle(TodoInCompletedEvent $event)
    {
        //we can fire with third party service in here
    }
}
