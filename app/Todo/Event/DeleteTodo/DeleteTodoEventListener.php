<?php


namespace App\Todo\Event\DeleteTodo;


use App\Todo\Contract\Service\TodoServiceInterface;
use App\Todo\Event\TodoDeleted\TodoDeletedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class DeleteTodoEventListener implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * @var TodoServiceInterface
     */
    private $todoService;

    /**
     * Create the event listener.
     *
     * @param TodoServiceInterface $todoService
     */
    public function __construct(TodoServiceInterface $todoService)
    {
        $this->todoService = $todoService;
    }

    /**
     * Handle the event.
     *
     * @param DeleteTodoEvent $event
     * @return void
     */
    public function handle(DeleteTodoEvent $event)
    {
        $this->todoService->delete(
            $event->id
        );
        event(new TodoDeletedEvent($event->id));
    }
}
