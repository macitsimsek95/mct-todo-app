<?php

namespace App\Todo\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Todo\Model\Todo
 *
 * @property int $id
 * @property string $name
 * @property int $user_id
 * @property bool $complete
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Todo\Model\Todo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Todo\Model\Todo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Todo\Model\Todo query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Todo\Model\Todo whereComplete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Todo\Model\Todo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Todo\Model\Todo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Todo\Model\Todo whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Todo\Model\Todo whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Todo\Model\Todo whereUserId($value)
 * @mixin \Eloquent
 */
class Todo extends Model
{

    /*
     * Fillable fields for protecting mass assignment vulnerability
     */
    protected $fillable = [
        'name',
        'user_id',
        'complete'
    ];

    /*
     * Eloquent attribute casting
     */
    protected $casts = [
        'complete' => 'boolean',
    ];
}
