<?php


namespace App\Todo\Contract\Service;


use App\Todo\Model\Todo;
use Illuminate\Database\Eloquent\Collection;

interface TodoServiceInterface
{
    /** Create a new todo
     * @param string $name
     * @param int $user_id
     * @return Todo
     */
    public function create(string $name, int $user_id): Todo;

    /**
     * @param int $user_id
     * @return Collection
     */
    public function get(int $user_id): Collection;

    /**
     * @param int $id
     * @return Todo
     */
    public function find(int $id): Todo;

    /** Update todo according to todo id
     * @param int $id
     * @param bool $complete
     * @return Todo
     */
    public function complete(int $id, bool $complete): Todo;

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool;
}
