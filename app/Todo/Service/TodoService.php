<?php


namespace App\Todo\Service;


use App\Infrastructure\Service\BaseService;
use App\Todo\Contract\Repository\TodoRepositoryInterface;
use App\Todo\Contract\Service\TodoServiceInterface;
use App\Todo\Model\Todo;
use Illuminate\Database\Eloquent\Collection;

class TodoService extends BaseService implements TodoServiceInterface
{
    /**
     * @var TodoRepositoryInterface
     */
    private $todoRepository;

    /**
     * TodoService constructor.
     * @param TodoRepositoryInterface $todoRepository
     */
    public function __construct(TodoRepositoryInterface $todoRepository)
    {
        $this->todoRepository = $todoRepository;
    }

    /**
     * @param int $user_id
     * @return Collection
     */
    public function get(int $user_id): Collection
    {
        return $this->todoRepository->where('user_id', $user_id);
    }

    /**
     * @param int $id
     * @return Todo
     */
    public function find(int $id): Todo
    {
        return $this->todoRepository->findOrFail($id);
    }

    /** Create a new todo
     * @param string $name
     * @param int $user_id
     * @return Todo
     */
    public function create(string $name, int $user_id): Todo
    {
        return $this->todoRepository->create([
            'name' => $name,
            'user_id' => $user_id
        ]);
    }

    /** Update todo according to todo id
     * @param int $id
     * @param bool $complete
     * @return Todo
     */
    public function complete(int $id, bool $complete): Todo
    {
        $todo = $this->todoRepository->findOrFail($id);
        $todo->complete = $complete;
        $todo->save();
        return $todo;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id) : bool
    {
        $todo = $this->todoRepository->findOrFail($id);
        $todo->delete();
        return true;
    }
}
