<?php

namespace App\Http\Controllers;

use App\Todo\Contract\Service\TodoServiceInterface;
use App\Todo\Event\CompleteTodo\CompleteTodoEvent;
use App\Todo\Event\CreateTodo\CreateTodoEvent;
use App\Todo\Event\DeleteTodo\DeleteTodoEvent;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class TodoController extends Controller
{
    /**
     * @var TodoServiceInterface
     */
    private $todoService;

    /**
     * TodoController constructor.
     * @param TodoServiceInterface $todoService
     */
    public function __construct(TodoServiceInterface $todoService)
    {
        $this->todoService = $todoService;
    }

    /**
     * View ToDos listing.
     *
     * @return View
     */
    public function index()
    {
        $todoList = $this->todoService->get(Auth::id());

        return view('todo.list', compact('todoList'));
    }

    /**
     * View Create Form.
     *
     * @return View
     */
    public function create()
    {
        return view('todo.create');
    }

    /**
     * Create new Todo.
     *
     * @param Request $request
     *
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required']);

        event(new CreateTodoEvent($request->get('name'), Auth::user()->id));

        return redirect('/todo')
            ->with('flash_notification.message', 'New todo created successfully')
            ->with('flash_notification.level', 'success');
    }

    /**
     * Toggle Status.
     *
     * @param $id
     *
     * @return RedirectResponse
     */
    public function update($id)
    {
        event(new CompleteTodoEvent($id));

        return redirect()
            ->route('todo.index')
            ->with('flash_notification.message', 'Todo updated successfully')
            ->with('flash_notification.level', 'success');
    }

    /**
     * Delete Todo.
     *
     * @param $id
     *
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        event(new DeleteTodoEvent($id));

        return redirect()
            ->route('todo.index')
            ->with('flash_notification.message', 'Todo deleted successfully')
            ->with('flash_notification.level', 'success');
    }
}
