<?php namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\User;
use App\User\Contract\Service\UserServiceInterface;
use App\User\Event\CreateUser\CreateUserEvent;
use App\User\Event\UpdateUser\UpdateUserEvent;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class UserController extends Controller
{
    /**
     * @var UserServiceInterface
     */
    private $userService;

    /**
     * Constructor method
     * @param UserServiceInterface $userService
     */
    public function __construct(UserServiceInterface $userService)
    {
        $this->middleware('auth', ['only' => ['edit', 'update']]);
        $this->userService = $userService;
    }

    /**
     * Show User Registration Form
     *
     * @return View
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Register User
     *
     * @param UserRequest $request
     * @return RedirectResponse
     */
    public function store(UserRequest $request)
    {
        event(new CreateUserEvent(
            $request->get('name'),
            $request->get('email'),
            $request->get('password')));

        return redirect('login')
            ->with('flash_notification.message', 'User registered successfully')
            ->with('flash_notification.level', 'success');
    }

    /**
     * Show User Profile
     *
     * @param User $user
     * @return View
     */
    public function edit(User $user)
    {
        return view('users.profile', compact('user'));
    }

    /**
     * Update User Profile
     *
     * @param User $user
     * @param UserRequest $request
     * @return RedirectResponse
     */
    public function update(User $user, UserRequest $request)
    {
        event(new UpdateUserEvent(
            $user->id,
            $request->get('name'),
            $request->get('email'),
            $request->get('password')
        ));

        return redirect('/todo')
            ->with('flash_notification.message', 'Profile updated successfully')
            ->with('flash_notification.level', 'success');
    }

}
