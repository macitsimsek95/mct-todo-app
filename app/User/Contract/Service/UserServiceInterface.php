<?php


namespace App\User\Contract\Service;


use App\User;

interface UserServiceInterface
{
    /** Create a new user
     * @param string $name
     * @param string $email
     * @param string $password
     * @return User
     */
    public function create(string $name, string $email, string $password): User;


    /** Update user according to user id
     * @param int $id
     * @param string $name
     * @param string $email
     * @param string $password
     * @return User
     */
    public function update(int $id, string $name, string $email, string $password): User;
}
