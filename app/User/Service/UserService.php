<?php


namespace App\User\Service;


use App\Infrastructure\Service\BaseService;
use App\User;
use App\User\Contract\Service\UserServiceInterface;
use App\User\Contract\UserRepositoryInterface;

/**
 * Class UserService
 * @package App\User\Service
 */
class UserService extends BaseService implements UserServiceInterface
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * UserService constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /** Create a new user
     * @param string $name
     * @param string $email
     * @param string $password
     * @return User
     */
    public function create(string $name, string $email, string $password): User
    {
        return $this->userRepository->create([
            'name' => $name,
            'email' => $email,
            'password' => bcrypt($password)
        ]);
    }

    /** Update user according to user id
     * @param int $id
     * @param string $name
     * @param string $email
     * @param string $password
     * @return User
     */
    public function update(int $id, string $name, string $email, string $password): User
    {
        $user = $this->userRepository->find($id);
        $user->name = $name;
        $user->email = $email;

        if ($password !== '') {
            $user->password = bcrypt($password);
        }
        $user->save();
        return $user;
    }
}
