<?php


namespace App\User\Repository;


use App\Infrastructure\BaseEloquentRepository;
use App\User;
use App\User\Contract\UserRepositoryInterface;

class UserRepository extends BaseEloquentRepository implements UserRepositoryInterface
{

    /**
     * UserRepository constructor.
     */
    public function __construct()
    {
        parent::__construct(User::class);
    }
}
