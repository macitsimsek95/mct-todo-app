<?php


namespace App\User\Event\UpdateUser;


use App\User\Contract\Service\UserServiceInterface;
use App\User\Event\UserUpdated\UserUpdatedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UpdateUserEventListener implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * @var UserServiceInterface
     */
    private $userService;

    /**
     * Create the event listener.
     *
     * @param UserServiceInterface $userService
     */
    public function __construct(UserServiceInterface $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Handle the event.
     *
     * @param UpdateUserEvent $event
     * @return void
     */
    public function handle(UpdateUserEvent $event)
    {
        $user = $this->userService->update(
            $event->user_id,
            $event->name,
            $event->email,
            $event->password);

        event(new UserUpdatedEvent($user));
    }
}
