<?php


namespace App\User\Event\CreateUser;

use App\User\Contract\Service\UserServiceInterface;
use App\User\Event\UserCreated\UserCreatedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CreateUserEventListener implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * @var UserServiceInterface
     */
    private $userService;

    /**
     * Create the event listener.
     *
     * @param UserServiceInterface $userService
     */
    public function __construct(UserServiceInterface $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Handle the event.
     *
     * @param CreateUserEvent $event
     * @return void
     */
    public function handle(CreateUserEvent $event)
    {
        $user = $this->userService->create(
            $event->name,
            $event->email,
            $event->password
        );
        event(new UserCreatedEvent($user));
    }
}
