<?php


namespace App\User\Event\UserUpdated;


use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UserUpdatedEventListener implements  ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param UserUpdatedEvent $event
     * @return void
     */
    public function handle(UserUpdatedEvent $event)
    {
        //we can handle and fire some event for third party services in here
    }
}
