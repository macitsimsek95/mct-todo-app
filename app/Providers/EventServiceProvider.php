<?php

namespace App\Providers;

use App\Todo\Event\CompleteTodo\CompleteTodoEvent;
use App\Todo\Event\CompleteTodo\CompleteTodoEventListener;
use App\Todo\Event\CreateTodo\CreateTodoEvent;
use App\Todo\Event\CreateTodo\CreateTodoEventListener;
use App\Todo\Event\DeleteTodo\DeleteTodoEvent;
use App\Todo\Event\DeleteTodo\DeleteTodoEventListener;
use App\Todo\Event\TodoCompleted\TodoCompletedEvent;
use App\Todo\Event\TodoCompleted\TodoCompletedEventListener;
use App\Todo\Event\TodoCreated\TodoCreatedEvent;
use App\Todo\Event\TodoCreated\TodoCreatedEventListener;
use App\Todo\Event\TodoDeleted\TodoDeletedEvent;
use App\Todo\Event\TodoDeleted\TodoDeletedEventListener;
use App\Todo\Event\TodoInCompleted\TodoInCompletedEvent;
use App\Todo\Event\TodoInCompleted\TodoInCompletedEventListener;
use App\User\Event\CreateUser\CreateUserEvent;
use App\User\Event\CreateUser\CreateUserEventListener;
use App\User\Event\UpdateUser\UpdateUserEvent;
use App\User\Event\UpdateUser\UpdateUserEventListener;
use App\User\Event\UserCreated\UserCreatedEvent;
use App\User\Event\UserCreated\UserCreatedEventListener;
use App\User\Event\UserUpdated\UserUpdatedEvent;
use App\User\Event\UserUpdated\UserUpdatedEventListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        CreateUserEvent::class => [
            CreateUserEventListener::class
        ],
        UpdateUserEvent::class => [
            UpdateUserEventListener::class
        ],
        UserCreatedEvent::class => [
            UserCreatedEventListener::class
        ],
        UserUpdatedEvent::class => [
            UserUpdatedEventListener::class
        ],
        CreateTodoEvent::class => [
            CreateTodoEventListener::class
        ],
        DeleteTodoEvent::class => [
            DeleteTodoEventListener::class
        ],
        CompleteTodoEvent::class => [
            CompleteTodoEventListener::class
        ],
        TodoCompletedEvent::class => [
            TodoCompletedEventListener::class
        ],
        TodoCreatedEvent::class => [
            TodoCreatedEventListener::class
        ],
        TodoDeletedEvent::class => [
            TodoDeletedEventListener::class
        ],
        TodoInCompletedEvent::class => [
            TodoInCompletedEventListener::class
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
