<?php

namespace App\Providers;

use App\Todo\Contract\Repository\TodoRepositoryInterface;
use App\Todo\Contract\Service\TodoServiceInterface;
use App\Todo\Repository\TodoRepository;
use App\Todo\Service\TodoService;
use App\User\Contract\Service\UserServiceInterface;
use App\User\Contract\UserRepositoryInterface;
use App\User\Repository\UserRepository;
use App\User\Service\UserService;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //repositories
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(TodoRepositoryInterface::class, TodoRepository::class);

        //services
        $this->app->bind(UserServiceInterface::class, UserService::class);
        $this->app->bind(TodoServiceInterface::class, TodoService::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

}
