<hr/>

<div class="container">
    &copy; {{ date('Y') }}, <a href="https://www.linkedin.com/in/macitsimsek/">Macit Simsek</a>
    <br/>
</div>
