@extends('layouts.master')

@section('content')
    <div class="text-center">
        <h1>Welcome to Macit Simsek ToDo App</h1>
        <hr/>

        @include('partials.flash_notification')

        <p>For any query please contact</p>

        <h3>Macit Simsek</h3>
        <h4><a href="https://www.linkedin.com/in/macitsimsek/">Contact</a></h4>
    </div>
@endsection
